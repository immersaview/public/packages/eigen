cmake_minimum_required(VERSION 3.0)

function(add_imported_lib LIB)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" INTERFACE IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")
    if (${CMAKE_VERSION} VERSION_GREATER 3.11.0)
        target_compile_definitions("${LIB}" INTERFACE EIGEN_MPL2_ONLY)
    endif ()
endfunction(add_imported_lib LIB)

add_imported_lib("eigen")