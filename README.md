**Nuget packaging for Eigen Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target eigen)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:eigen,INTERFACE_INCLUDE_DIRECTORIES>")
```
